const jwt = require('jsonwebtoken');
const config = require('../config');

function jwtMiddleware(req, res, next) {

  if (!req.path.startsWith('/v1/users')) {

    if (!req.headers.authorization) {
      res.sendStatus(401);
    }

    console.log('------------------' + req.method + ' ' + req.path + '-------------------');
    console.log(req.headers);
    console.log('----------------------------------------------------');

    let token = req.headers.authorization.split(' ')[1];

    jwt.verify(token, config.secret, function (err, decodedToken) {
      if (!err) {
        req.tokenData = decodedToken;
      } else {
        res.sendStatus(403);
      }
    });
  }

  return next();
}

module.exports = jwtMiddleware;