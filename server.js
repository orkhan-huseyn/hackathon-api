const express = require('express');
const bodyParser = require('body-parser');
const jwtMiddleware = require('./middleware/jwt');
const cors = require('cors');

const app = express();

//// external routes
const users = require('./routes/v1/users');
const transactions = require('./routes/v1/transactions');
const periods = require('./routes/v1/periods');
const cards = require('./routes/v1/cards');
const templates = require('./routes/v1/templates');
const invoices = require('./routes/v1/invoices');
const recepients = require('./routes/v1/recepients');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
// midleware to verrify jwt token
app.use(jwtMiddleware);

app.use('/v1/users', users);
app.use('/v1/cards', cards);
app.use('/v1/periods', periods);
app.use('/v1/invoices', invoices);
app.use('/v1/templates', templates);
app.use('/v1/recepients', recepients);
app.use('/v1/transactions', transactions);

app.listen(8080, function () {
  console.log('Hackathon API is running...');
});