const config = {
  db: {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'hackathon'
  },
  env: 'dev',
  secret: 'Asan1234'
};

module.exports = config;