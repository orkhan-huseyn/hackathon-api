const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const config = require('../../config');
const connection = require('../../database');
const router = express.Router();

// getting list of users
router.get('/', function (req, res) {

  connection.query('SELECT id, first_name, last_name, pin  FROM user', function (err, result) {
    if (err) throw err;
    res.send(result);
  });

});

// inserting new user to database
router.post('/', function (req, res) {

  const user = {
    first_name: req.body.firstName,
    last_name: req.body.lastName,
    pin: req.body.pin,
  };

  bcrypt.hash(req.body.password, 5, function (error, hash) {

    user.password = hash;

    connection.query('INSERT INTO user SET ?', user, function (err, result) {
      if (err) throw err;
      res.send({
        success: true
      });
    });

  });

});

// authenticating user and giving token
router.post('/authentication', function (req, res) {

  let pin = req.body.pin.toLowerCase();

  // check pin with the lower case letters too
  connection.query('SELECT password, first_name, last_name, id FROM user WHERE LOWER(pin)=?', [pin], function (err, result) {
    if (err) throw err;
    let message = 'İstifadəçi adı və ya şifrə yanlışdır.';

    if (result.length) {

      bcrypt.compare(req.body.password, result[0].password, function (er, passwordsMatch) {

        if (passwordsMatch) {

          let userData = {
            fullName: `${result[0].first_name} ${result[0].last_name}`,
            userId: result[0].id
          };

          jwt.sign({ userData }, config.secret, { expiresIn: 60 * 60 }, function (e, token) {

            res.send({ token: token });

          });
        } else {
          res.send({ success: false, message: message });
        }

      });

    } else {
      res.send({ success: false, message: message })
    }
  });

});

module.exports = router;