const express = require('express');
const router = express.Router();
const connection = require('../../database');

// define the home page route
router.get('/', function (req, res) {

  connection.query('SELECT id as value, name as text, label FROM period', function (err, result) {
    if (err) throw err;
    res.send(result);
  });

});

router.post('/', function (req, res) {
  res.send(req.body);
});

module.exports = router;