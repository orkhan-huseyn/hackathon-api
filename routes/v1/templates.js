const express = require('express');
const router = express.Router();
const connection = require('../../database');

// define the home page route
router.get('/', function (req, res) {

  connection.query(
    'SELECT t.id as id, t.name as name, t.amount as amount, p.name as period_name, r.name as recipient_name ' +
    'FROM template t ' +
    'INNER JOIN recipient r ON t.recipient_id=r.id ' +
    'INNER JOIN period p ON p.id=t.period_id WHERE t.user_id = ?',
    [req.tokenData.userData.userId],
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });

});

router.post('/', function (req, res) {

  let template = {
    user_id: req.tokenData.userData.userId,
    name: req.body.title,
    recipient_id: req.body.receiver,
    amount: req.body.amount,
    period_id: req.body.periodType
  };

  connection.query('INSERT INTO template SET ?', template, function (err, result) {

    if (err) throw err;

    res.send({
      success: true
    });

  });
});

module.exports = router;