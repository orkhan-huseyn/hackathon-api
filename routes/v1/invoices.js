const express = require('express');
const router = express.Router();
const connection = require('../../database');

// define the home page route
router.get('/', function (req, res) {

  connection.query(
    'SELECT i.id as id, i.name as name, i.description as description, i.amount as amount, i.last_date as last_date,' +
    ' r.name as recipient_name ' +
    'FROM invoice i ' +
    'INNER JOIN recipient r ON r.id=i.recipient_id WHERE i.user_id = ?',
    [req.tokenData.userData.userId],
    function (err, result) {
      if (err) throw err;
      res.send(result);
    });

});

router.post('/', function (req, res) {
  res.send(req.body);
});

module.exports = router;